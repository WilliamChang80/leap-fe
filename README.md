# Learning Portal FrontEnd

Our open source project is initially inspired by our passion for digitalization of organization in BINUS University.

It is a learning management system that hopefully can optimize efficiency of organizations in managing their learning classes.

Our open source project focuses on four aspects: course management, attendance management, project assessment, documentation and reports.

We set up three different users, namely members, teachers, and admin.

Sample program is run here : [leap.christopheralvin.com](https://leap.christopheralvin.com/)

## 👨‍💻 Technologies

This project is created with:

- Jquery 3.4.1
- Axios 0.19.0
- Fontawesome 5.11.2
- Bootstrap 4.3.1
- HTML 5
- CSS 3
- Javascript

## 🤝 Contributing

Feel free to contribute on this project by following guideline below!

- Fork this repository

- Create merge request to master branch

- Fill the title with your changes

- Fill the description and describe your changes

## ⚙️ Submitting Issue

When you find some issue, you can submit an issue to help this project

- Fill the title with the brief problem

- Describe the problem and how to reproduce it

- Alternatively, you can suggest some of your ideas to solve the problem

## Another Project to contribute

If you are interested to contribute to our backend project, you can check [here](https://gitlab.com/rnddev/leap-be).
