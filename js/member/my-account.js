import {logout} from '../service/auth.js'
import {isMember} from '../service/middleware.js'
import {config, redirectTo} from '../service/config.js'
import { auth, member, route } from '../service/url.js'

const submitChangeEmail = () => {
    let email = $("#emailInput").val()
    axios.post(auth["change-email"], {
		'email': email,
	}, config.auth)
	.then(function (response) {
        redirectTo(route.member["my-account"])
	})
	.catch(function (error) {
        alert(error.response.data.email)
        redirectTo(route.member["my-account"])
		console.log(error.response.data.email)
	});
}

const submitChangePassword = () => {
    let old_password = $("input[name*='old-password']").val()
    let new_password = $("input[name*='new-password']").val()
    let new_password_confirmation = $("input[name*='confirm-password']").val()
    axios.post(auth["change-password"], {
        'old_password': old_password,
        'new_password': new_password,
        'new_password_confirmation': new_password_confirmation,
	}, config.auth)
	.then(function (response) {
        alert(response.data.success)
        redirectTo(route.member["my-account"])
	})
	.catch(function (error) {
        if(error.response.data.old_password) {
            alert(error.response.data.old_password)
            console.log(error.response.data.old_password)
        } else if(error.response.data.new_password) {
            alert(error.response.data.new_password)
            console.log(error.response.data.new_password)
        }
        redirectTo(route.member["my-account"])
	});
}

$(document).ready(function () {
    const btn = $('.btnEdit');
    const item = $('#email');
    const itemInput = $('#email-edit');
    const emailPrev = $('#emailPrevious');
    const DISPLAY_FLEX = {
        "display": "flex"
    };
    const DISPLAY_NONE = {
        "display": "none"
    };
    const DISPLAY_BLOCK = {
        "display": "block"
    };

    const changeEdit = function () {
        btnCalRemover(btn);
        itemInput.css(DISPLAY_FLEX);
        item.css(DISPLAY_NONE);
        $('.btnSaveProfile').css(DISPLAY_BLOCK);
        emailPrev.html("Previous: " + item.html())
    }

    const btnCalRemover = function (btnLocal) {
        const height = parseInt(btnLocal.css("height")) + parseInt(btnLocal.css("margin-bottom")) + parseInt(btnLocal.css("margin-top"));
        btnLocal.css(DISPLAY_NONE);
        btnLocal.parent().css({
            "height": "" + height + "px"
        });
    }

    const saveBtn = function () {
        submitChangeEmail()
        btn.css(DISPLAY_BLOCK);
        $('.btnSaveProfile').css(DISPLAY_NONE);
        itemInput.css(DISPLAY_NONE);
        item.css(DISPLAY_BLOCK);
    }

    btn.click(changeEdit);
    $('.btnSaveProfile').click(saveBtn);
})

const parseDateString = (s) => {
    var b = s.split(/\D/);
    var d = new Date(b[0], --b[1], b[2], b[3], b[4], b[5]);
    const month = d.toLocaleString('default', { month: 'long' });
    return month + " " + d.getFullYear();
}

async function getMemberData() {
	await axios.get(member.details, 
		config.auth
	)
		.then((response) => {
        let user_id = response.data.success.name
        let email = response.data.success.email
        let major_id = response.data.success.major_name
        let member_name = response.data.success.name
        let d = response.data.success.created_at

        $('#major').html(major_id)
        $('#user-id').html(user_id)
        $('#email').html(email)
        $('#member-name').html("Hey, "+member_name+"!")
        $('#join-since').html("Joined "+parseDateString(d))
	}).catch(function (error) {
		console.log(error);
	});
}

const navigation = () =>{
    $('#btn-view-class1').click(function() {
        redirectTo(route.member["view-class"])
    });

    $('#btn-view-class2').click(function() {
        redirectTo(route.member["view-class"])
    });

    $('#btn-view-material1').click(function() {
        redirectTo(route.member["view-material"])
    });

    $('#btn-view-material2').click(function() {
        redirectTo(route.member["view-material"])
    });

    $('#btn-upload-project1').click(function() {
        redirectTo(route.member["upload-project"])
    });

    $('#btn-upload-project2').click(function() {
        redirectTo(route.member["upload-project"])
    });

    // $('#btn-attendance1').click(function() {
    //     // redirectTo(route.member["upload-project"])
    // });

    // $('#btn-attendance2').click(function() {
    //     // redirectTo(route.member["upload-project"])
    // });
}

$(document).ready(function() {
    if(!isMember()){
        alert("You Are Not Allowed to Access This Page")
        logout()
    } else {
        getMemberData()
        navigation()
        $('#btn-save-new-password').click(submitChangePassword);
    }
})

