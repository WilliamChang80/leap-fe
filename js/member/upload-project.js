import { logout } from "../service/auth.js";
import { isMember } from "../service/middleware.js";
import { config, redirectTo, urlTo } from "../service/config.js";
import { member, route, host } from "../service/url.js";

const navigation = () => {
  $("#btn-view-class1").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-class2").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-material1").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-view-material2").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-upload-project1").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-upload-project2").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-attendance1").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-attendance2").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-view-class1").attr("href", urlTo(route.member["view-class"]));
  $("#btn-view-class2").attr("href", urlTo(route.member["view-class"]));

  $("#btn-view-material1").attr("href", urlTo(route.member["view-material"]));
  $("#btn-view-material2").attr("href", urlTo(route.member["view-material"]));

  $("#btn-upload-project1").attr("href", urlTo(route.member["upload-project"]));
  $("#btn-upload-project2").attr("href", urlTo(route.member["upload-project"]));

  $("#btn-attendance1").attr("href", urlTo(route.member.attendance));
  $("#btn-attendance2").attr("href", urlTo(route.member.attendance));
};

const divMidProject = () => {
  return `<div class="col-lg-8 col-md-10 col-sm-10 col-10 con-base" style="margin-bottom: 20px;">
                <div class="container con-info">
                <div class="col-lg-8 col-md-12 col-sm-12 uploadInfoLeft">
                    <div class="titleUploadProject">
                        <h1 id="mid-project-title">Mid-Year Project</h1>
                    </div>
                    <div class="spaceBetweenInfo">
                        <h5>Requirement:</h5>
                    </div>
                    <div>
                    <p id="mid-project-requirement">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Doloremque, accusantium quisquam neque maiores reiciendis
                        culpa illo fuga adipisci nam obcaecati atque non iste hic
                        nihil consequatur quaerat sit ipsam. Esse.
                    </p>
                    </div>
                    <div class="spaceBetweenInfo">
                        <h5>Judgement Criteria:</h5>
                    </div>
                    <div>
                        <p id="mid-project-judgement">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Perspiciatis tenetur, impedit dolore accusantium adipisci ad
                            cum molestiae laborum quos, tempora ex provident nisi
                            repudiandae, modi voluptates? Autem sint quo aspernatur.
                        </p>
                    </div>
                    <a href="#" id="mid-project-download">Download Mid Project Case</a>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 uploadInfoRight">
                    <div>
                    <button
                        type="button"
                        class="btn btn-upload-submit"
                        data-toggle="modal"
                        data-target="#myModal"
                    >
                        <h4>Submit</h4>
                    </button>
                    </div>
                    <div>
                    <p id="lastSubmitText">Last Submitted:</p>
                    </div>
                    <div>
                    <a href="#" id="submitted-mid">Download</a>
                    </div>
                </div>
                </div>
            </div>`;
};

const divFinalProject = () => {
  return `<div class="col-lg-8 col-md-10 col-sm-10 col-10 con-base" style="margin-bottom: 20px;">
                <div class="container con-info">
                <div class="col-lg-8 col-md-12 col-sm-12 uploadInfoLeft">
                    <div class="titleUploadProject">
                        <h1 id="final-project-title">Mid-Year Project</h1>
                    </div>
                    <div class="spaceBetweenInfo">
                        <h5>Requirement:</h5>
                    </div>
                    <div>
                    <p id="final-project-requirement">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Doloremque, accusantium quisquam neque maiores reiciendis
                        culpa illo fuga adipisci nam obcaecati atque non iste hic
                        nihil consequatur quaerat sit ipsam. Esse.
                    </p>
                    </div>
                    <div class="spaceBetweenInfo">
                        <h5>Judgement Criteria:</h5>
                    </div>
                    <div>
                        <p id="final-project-judgement">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                            Perspiciatis tenetur, impedit dolore accusantium adipisci ad
                            cum molestiae laborum quos, tempora ex provident nisi
                            repudiandae, modi voluptates? Autem sint quo aspernatur.
                        </p>
                    </div>
                    <a href="#" id="final-project-download">Download Final Project Case</a>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 uploadInfoRight">
                    <div>
                    <button
                        type="button"
                        class="btn btn-upload-submit"
                        data-toggle="modal"
                        data-target="#myModal2"
                    >
                        <h4>Submit</h4>
                    </button>
                    </div>
                    <div>
                    <p id="lastSubmitText">Last Submitted:</p>
                    </div>
                    <div>
                    <a href="#" id="submitted-final">Download</a>
                    </div>
                </div>
                </div>
            </div>`;
};

const getMemberAssignment = () => {
  axios
    .get(member["get-user-assignment"], config.auth)
    .then(response => {
      let content = divMidProject() + divFinalProject();

      $(content).insertAfter("#carouselId");

      console.log(response.data.data);

      let mid_exam_name = response.data.data.mid_exam_name;
      let mid_requirement = response.data.data.mid_requirement;
      let mid_exam_judgement_criteria =
        response.data.data.mid_judgment_criteria;
      let mid_exam_file = response.data.data.mid_exam_file;

      let final_exam_name = response.data.data.final_exam_name;
      let final_requirement = response.data.data.final_requirement;
      let final_exam_judgement_criteria =
        response.data.data.final_judgment_criteria;
      let final_exam_file = response.data.data.final_exam_file;

      $("#mid-project-title").html(mid_exam_name);
      $("#mid-project-requirement").html(mid_requirement);
      $("#mid-project-judgement").text(mid_exam_judgement_criteria);

      $("#mid-project-download").click(function() {
        let download_link = host + "/storage/" + mid_exam_file;
        let a = document.createElement("a");
        a.target = "_blank";
        a.href = download_link;
        a.click();
      });

      $("#final-project-title").html(final_exam_name);
      $("#final-project-requirement").html(final_requirement);
      $("#final-project-judgement").text(final_exam_judgement_criteria);

      $("#final-project-download").click(function() {
        let download_link = host + "/storage/" + final_exam_file;
        let a = document.createElement("a");
        a.target = "_blank";
        a.href = download_link;
        a.click();
      });

      let submitted_mid = response.data.submitted_assignment.mid_exam_file;
      let submitted_final = response.data.submitted_assignment.final_exam_file;

      $("#submitted-mid").click(function() {
        let download_link = host + "/storage/" + submitted_mid;
        let a = document.createElement("a");
        a.target = "_blank";
        a.href = download_link;
        a.click();
      });

      $("#submitted-final").click(function() {
        let download_link = host + "/storage/" + submitted_final;
        let a = document.createElement("a");
        a.target = "_blank";
        a.href = download_link;
        a.click();
      });
    })
    .catch(function(error) {
      console.log(error);
    });
};

const submitMidProject = () => {
  var formData = new FormData();
  var mid_file = document.querySelector("#mid-file");
  formData.append("mid_exam_file", mid_file.files[0]);

  axios
    .post(member["submit-mid-project"], formData, config["auth-with-file"])
    .then(function(response) {
      alert("Mid Projet Submmited");
      redirectTo(route.member["upload-project"]);
    })
    .catch(function(error) {
      console.log(error);
    });
};

const submitFinalProject = () => {
  var formData = new FormData();
  var final_file = document.querySelector("#final-file");
  formData.append("final_exam_file", final_file.files[0]);

  axios
    .post(member["submit-final-project"], formData, config["auth-with-file"])
    .then(function(response) {
      alert("Final Projet Submmited");
      redirectTo(route.member["upload-project"]);
    })
    .catch(function(error) {
      console.log(error);
    });
};

$(document).ready(function() {
  if (!isMember()) {
    alert("You Are Not Allowed to Access This Page");
    logout();
  } else {
    getMemberAssignment();
    navigation();

    $("#mid-file").on("change", function() {
      var fileName = $(this)
        .val()
        .split("\\")
        .pop();
      $("#mid-file-name").val(fileName);
    });

    $("#final-file").on("change", function() {
      var fileName = $(this)
        .val()
        .split("\\")
        .pop();
      $("#final-file-name").val(fileName);
    });

    $("#submit-mid-project").on("submit", function() {
      submitMidProject();
      return false;
    });

    $("#submit-final-project").on("submit", function() {
      submitFinalProject();
      return false;
    });
  }
});
function addEventHandler(elem, eventType, handler) {
  if (elem.addEventListener) elem.addEventListener(eventType, handler, false);
  else if (elem.attachEvent) elem.attachEvent("on" + eventType, handler);
}
addEventHandler(document, "DOMContentLoaded", function() {
  addEventHandler(document.getElementById("mid-file"), "change", function() {
    var file = this.value;
    if (file) {
      var startIndex =
        file.indexOf("\\") >= 0
          ? file.lastIndexOf("\\")
          : file.lastIndexOf("/");
      var filename = file.substring(startIndex);
      if (filename.indexOf("\\") === 0 || filename.indexOf("/") === 0) {
        filename = filename.substring(1);
      }
    }
    document.getElementById("mid-file-name").innerHTML = filename;
  });
  addEventHandler(document.getElementById("final-file"), "change", function() {
    var file = this.value;
    if (file) {
      var startIndex =
        file.indexOf("\\") >= 0
          ? file.lastIndexOf("\\")
          : file.lastIndexOf("/");
      var filename = file.substring(startIndex);
      if (filename.indexOf("\\") === 0 || filename.indexOf("/") === 0) {
        filename = filename.substring(1);
      }
    }
    document.getElementById("final-file-name").innerHTML = filename;
  });
});
