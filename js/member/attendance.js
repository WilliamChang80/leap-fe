import { logout } from "../service/auth.js";
import { isMember } from "../service/middleware.js";
import { config, redirectTo, urlTo } from "../service/config.js";
import { member, route } from "../service/url.js";
import { getNextClass, getPreviousClass } from "../member/view-class.js";

const navigation = () => {
  $("#btn-view-class1").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-class2").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-material1").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-view-material2").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-upload-project1").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-upload-project2").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-attendance1").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-attendance2").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-view-class1").attr("href", urlTo(route.member["view-class"]));
  $("#btn-view-class2").attr("href", urlTo(route.member["view-class"]));

  $("#btn-view-material1").attr("href", urlTo(route.member["view-material"]));
  $("#btn-view-material2").attr("href", urlTo(route.member["view-material"]));

  $("#btn-upload-project1").attr("href", urlTo(route.member["upload-project"]));
  $("#btn-upload-project2").attr("href", urlTo(route.member["upload-project"]));

  $("#btn-attendance1").attr("href", urlTo(route.member.attendance));
  $("#btn-attendance2").attr("href", urlTo(route.member.attendance));
};

async function getUserAbsence() {
  await axios
    .get(member["user-absences"], config.auth)
    .then(response => {
      for (let j = 0; j < response.data.absence.length; j++) {
        let idx = j + 1;
        let PRESENT = 1;
        let ABSENT = 0;

        if (response.data.absence[j].absence == PRESENT) {
          $("#prev-meeting-lecturer" + idx).html("<b>Present</b>");
        } else {
          $("#prev-meeting-lecturer" + idx).html("<b>Absent</b>");
          $("#prev" + idx).attr("class", "classBar2");
        }
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

async function getUserAbsenceCount() {
  axios
    .get(member["user-absences-count"], config.auth)
    .then(response => {
      let present_count = response.data.present;
      let absence_count = response.data.total;

      $("#present-count").html(present_count);
      $("#absence-count").html(absence_count);
    })
    .catch(function(error) {
      console.log(error);
    });
}

async function getUserClassCount() {
  axios
    .get(member["user-classes-count"], config.auth)
    .then(response => {
      let class_count = response.data.class_count;

      $("#class-count").html(class_count);
    })
    .catch(function(error) {
      console.log(error);
    });
}
$(document).ready(function() {
  if (!isMember()) {
    alert("You Are Not Allowed to Access This Page");
    logout();
  } else {
    navigation();
    getUserAbsenceCount();
    getUserClassCount();
    getPreviousClass("#previous-class-content");
    // getNextClass();
    getUserAbsence();
  }
});

export { getUserAbsenceCount, getUserClassCount };
