import { logout } from "../service/auth.js";
import { isMember } from "../service/middleware.js";
import { config, redirectTo, urlTo } from "../service/config.js";
import { member, route, host } from "../service/url.js";

const parseDateString = s => {
  var b = s.split(/\D/);
  var d = new Date(b[0], --b[1], b[2]);
  const month = d.toLocaleString("default", { month: "short" });
  return d.getDate() + " " + month + " " + d.getFullYear();
};

const divPreviousCLass = x => {
  return `<div class="classBar2" id="prevClass${x}">
                <p class="barItem" id="nextClass1">PREVIOUS CLASS</p>
                <p class="barItem" id="meeting-name${x}">Photoshop</p>
                <button class="btn0" id="btn-download${x}">Download</button>
            </div>`;
};

const divNextClass = x => {
  return `<div class="classBar">
                <p class="barItem" id="nextClass">NEXT CLASS</p>
                <p class="barItem" id="meeting-name${x}">Wireframing</p>
                <button class="btn" id="btn-download${x}">Download</button>
            </div>`;
};

const divUpcomingClass = (x, idx) => {
  return `<div class="upcomingClass" id="upcomingClass${x}">
                <p class="barItem" id="nextClass">UPCOMING CLASS</p>
                <p class="barItem" id="meeting-name${idx}">Mobile Design</p>
                <button class="btn1" id="btn-download${idx}">Download</button>
            </div>`;
};

const getAllMaterial = () => {
  axios
    .get(member["all-class-material"], config.auth)
    .then(response => {
      let prev_length = response.data.prev_classes.length;
      let next_length = response.data.next_classes.length;

      //loop 6x dummy
      let content = "";
      for (let i = 1; i <= prev_length + next_length; i++) {
        if (i <= prev_length) {
          content += divPreviousCLass(i);
        } else if (i == prev_length + 1) {
          content += divNextClass(i);
        } else {
          content += divUpcomingClass(i - prev_length + 1, i);
        }
      }

      $(content).insertAfter("#leftPath2");

      // append data
      for (let j = 0; j < prev_length + next_length; j++) {
        let idx = j + 1;
        if (j <= prev_length - 1) {
          console.log("a");
          let meeting_name = response.data.prev_classes[j].meeting_name;
          $("#meeting-name" + idx).html(meeting_name);
          $("#btn-download" + idx).click(function() {
            let download_link =
              host + "/storage/" + response.data.prev_classes[j].material;
            let a = document.createElement("a");
            a.target = "_blank";
            a.href = download_link;
            a.click();
          });
        } else {
          console.log("b");
          let meeting_name =
            response.data.next_classes[j - prev_length].meeting_name;
          $("#meeting-name" + idx).html(meeting_name);
          $("#btn-download" + idx).click(function() {
            let download_link =
              host +
              "/storage/" +
              response.data.next_classes[j - prev_length].material;
            let a = document.createElement("a");
            a.target = "_blank";
            a.href = download_link;
            a.click();
          });
        }
      }
    })
    .catch(function(error) {
      console.log(error);
    });
};

const navigation = () => {
  $("#btn-view-class1").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-class2").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-material1").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-view-material2").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-upload-project1").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-upload-project2").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-attendance1").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-attendance2").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-view-class1").attr("href", urlTo(route.member["view-class"]));
  $("#btn-view-class2").attr("href", urlTo(route.member["view-class"]));

  $("#btn-view-material1").attr("href", urlTo(route.member["view-material"]));
  $("#btn-view-material2").attr("href", urlTo(route.member["view-material"]));

  $("#btn-upload-project1").attr("href", urlTo(route.member["upload-project"]));
  $("#btn-upload-project2").attr("href", urlTo(route.member["upload-project"]));

  $("#btn-attendance1").attr("href", urlTo(route.member.attendance));
  $("#btn-attendance2").attr("href", urlTo(route.member.attendance));
};

$(document).ready(function() {
  if (!isMember()) {
    alert("You Are Not Allowed to Access This Page");
    logout();
  } else {
    getAllMaterial();
    navigation();
  }
});
