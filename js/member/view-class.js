import { logout } from "../service/auth.js";
import { isMember } from "../service/middleware.js";
import { config, redirectTo, urlTo } from "../service/config.js";
import { member, route } from "../service/url.js";

const parseDateString = s => {
  var b = s.split(/\D/);
  var d = new Date(b[0], --b[1], b[2]);
  const month = d.toLocaleString("default", { month: "short" });
  return d.getDate() + " " + month + " " + d.getFullYear();
};

const divPreviousClass = x => {
  return `<div class="classBar3" id="prev${x}">
                <p class="barItem" id="nextClass1">PREVIOUS CLASS</p>
                <p class="barItem" id="prev-meeting-name${x}">--------</p>
                <p class="barItem" id="prev-meeting-date${x}">1 Jan 1970, 00.00-00.00</p>
                <p class="barItem" id="prev-meeting-lecturer${x}">-------</p>
            </div>`;
};

const getPreviousClass = async pos => {
  axios
    .get(member["previous-class"], config.auth)
    .then(response => {
      //loop 6x dummy
      let content = "";
      for (let i = 1; i <= response.data.prev_classes.length; i++) {
        content += divPreviousClass(i);
      }

      $(content).insertAfter(pos);

      //append data
      for (let j = 0; j < response.data.prev_classes.length; j++) {
        let meeting_date = response.data.prev_classes[j].meeting_date;
        let lecturer_name = response.data.prev_classes[j].lecturer_name;
        let day = response.data.prev_classes[j].day;
        let begin_time = response.data.prev_classes[j].begin_time;
        let end_time = response.data.prev_classes[j].end_time;
        let b_time = begin_time.split(":");
        let e_time = end_time.split(":");
        let shift =
          b_time[0] + "." + b_time[1] + "-" + e_time[0] + "." + e_time[1];

        let meeting_name = response.data.prev_classes[j].meeting_name;

        let d = parseDateString(meeting_date) + ", " + shift;

        let idx = j + 1;
        $("#prev-meeting-date" + idx).html(d);
        $("#prev-meeting-lecturer" + idx).html(lecturer_name);
        $("#prev-meeting-name" + idx).html(meeting_name);
      }
    })
    .catch(function(error) {
      console.log("error");
      console.log(error);
    });
};

const divNextClass = x => {
  return `<div class="classBar">
                <p class="barItem" id="nextClass">NEXT CLASS</p>
                <p class="barItem" id="meeting-name${x}">--------</p>
                <p class="barItem" id="meeting-date${x}">1 Jan 1970, 00.00-00.00</p>
                <p class="barItem" id="meeting-lecturer${x}">-------</p>
            </div>`;
};

const divUpcomingClass = x => {
  return `<div class="upcomingClass" id="upcomingClass${x - 1}">
                <p class="barItem" id="upcomingClass">UPCOMING CLASS</p>
                <p class="barItem" id="meeting-name${x}">--------</p>
                <p class="barItem" id="meeting-date${x}">1 Jan 1970, 00.00-00.00</p>
                <p class="barItem" id="meeting-lecturer${x}">-------</p>
            </div>`;
};

const getNextClass = async pos => {
  axios
    .get(member["next-class"], config.auth)
    .then(response => {
      //loop 6x dummy
      let content = "";
      for (let i = 1; i <= response.data.next_classes.length; i++) {
        if (i == 1) {
          content += divNextClass(i);
        } else {
          content += divUpcomingClass(i);
        }
      }

      $(content).insertAfter(pos);

      //append data
      for (let j = 0; j < response.data.next_classes.length; j++) {
        let meeting_date = response.data.next_classes[j].meeting_date;
        let lecturer_name = response.data.next_classes[j].lecturer_name;
        let day = response.data.next_classes[j].day;
        let begin_time = response.data.next_classes[j].begin_time;
        let end_time = response.data.next_classes[j].end_time;
        let location_name = response.data.next_classes[j].location_name;
        let b_time = begin_time.split(":");
        let e_time = end_time.split(":");
        let shift =
          b_time[0] + "." + b_time[1] + "-" + e_time[0] + "." + e_time[1];

        let meeting_name = response.data.next_classes[j].meeting_name;

        let d = parseDateString(meeting_date) + ", " + shift;

        let idx = j + 1;
        $("#meeting-date" + idx).html(d);
        $("#meeting-lecturer" + idx).html(lecturer_name);
        $("#meeting-name" + idx).html(meeting_name);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
};

const navigation = () => {
  $("#btn-view-class1").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-class2").click(function() {
    redirectTo(route.member["view-class"]);
  });

  $("#btn-view-material1").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-view-material2").click(function() {
    redirectTo(route.member["view-material"]);
  });

  $("#btn-upload-project1").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-upload-project2").click(function() {
    redirectTo(route.member["upload-project"]);
  });

  $("#btn-attendance1").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-attendance2").click(function() {
    redirectTo(route.member.attendance);
  });

  $("#btn-view-class1").attr("href", urlTo(route.member["view-class"]));
  $("#btn-view-class2").attr("href", urlTo(route.member["view-class"]));

  $("#btn-view-material1").attr("href", urlTo(route.member["view-material"]));
  $("#btn-view-material2").attr("href", urlTo(route.member["view-material"]));

  $("#btn-upload-project1").attr("href", urlTo(route.member["upload-project"]));
  $("#btn-upload-project2").attr("href", urlTo(route.member["upload-project"]));

  $("#btn-attendance1").attr("href", urlTo(route.member.attendance));
  $("#btn-attendance2").attr("href", urlTo(route.member.attendance));
};

$(document).ready(function() {
  if (!isMember()) {
    alert("You Are Not Allowed to Access This Page");
    logout();
  } else {
    getNextClass("#next-class-content");
    navigation();
  }
});

export { getNextClass, getPreviousClass };
