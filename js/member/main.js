import {logout} from '../service/auth.js'
import {isMember} from '../service/middleware.js'
import {config, redirectTo} from '../service/config.js'
import {member, route} from '../service/url.js'

async function getUserAbsenceCount() {
    axios.get(member["user-absences-count"], 
		config.auth
	)
		.then((response) => {
        let present_count = response.data.present
        let absence_count = response.data.total
        
        $('#present-count').html(present_count)
        $('#absence-count').html(absence_count)

	}).catch(function (error) {
		console.log(error);
	});
}

async function getUserClassCount() {
    axios.get(member["user-classes-count"], 
		config.auth
	)
		.then((response) => {
        let class_count = response.data.class_count

        $('#class-count').html(class_count)

        getUserAbsenceCount()

	}).catch(function (error) {
		console.log(error);
	});
}

const parseDateString = (s) => {
    var b = s.split(/\D/); 
    var d = new Date(b[0], --b[1], b[2]);
    const month = d.toLocaleString('default', { month: 'short' });
    return d.getDate() + " " + month + " " + d.getFullYear();
}

async function getNextClass() {
	axios.get(member["next-class"], 
		config.auth
	)
		.then((response) => {
            
        let meeting_date = response.data.next_classes[0].meeting_date
        
        let lecturer_name = response.data.next_classes[0].lecturer_name
        let day = response.data.next_classes[0].day
        let begin_time = response.data.next_classes[0].begin_time
        let end_time = response.data.next_classes[0].end_time
        
        let b_time = begin_time.split(":")
        let e_time = end_time.split(":")
        let shift = b_time[0] + "." +b_time[1]+"-"+e_time[0] + "." +e_time[1]
        let meeting_name = response.data.next_classes[0].meeting_name

        let d = parseDateString(meeting_date) + ", " + shift
        
        let course_name = response.data.course_name
        let location = response.data.location

        $('#meeting-date').html(d)
        $('#day').html(day+", ")
        $('#shift').html(shift)
        $('#location').html(location)
        $('#lecturer').html(lecturer_name)
        $('#meeting-lecturer').html(lecturer_name)
        $('#meeting-name').html(meeting_name)
        $('#course-name').html(course_name)

	}).catch(function (error) {
		console.log(error);
	});
}

async function getMemberData() {
	axios.get(member.details, 
		config.auth
	)
		.then((response) => {
        let name = response.data.success.name
        $('#nama-member').html(name)
        
	}).catch(function (error) {
		console.log(error);
	});
}

$(document).ready(function() {
    if(!isMember()){
        alert("You Are Not Allowed to Access This Page")
        logout()
    } else {
        getMemberData()
        getNextClass()
        getUserClassCount()
        // getUserAbsenceCount()
    }
})

