import {getCookie} from './config.js'

let isLecturer = (name) => {
    if(getCookie("role") == null || getCookie("role") == '' || getCookie("role") != 'lecturer'){
        return false
    } else {
        return true
    }
}

let isMember = (name) => {
    if(getCookie("role") == null || getCookie("role") == '' || getCookie("role") != 'member'){
        return false
    } else {
        return true
    }
}

let isAdmin = (name) => {
    if(getCookie("role") == null || getCookie("role") == '' || getCookie("role") != 'admin'){
        return false
    } else {
        return true
    }
}

export {isLecturer, isAdmin, isMember}