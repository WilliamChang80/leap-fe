import { auth, route } from "./url.js";
import {
  config,
  redirectTo,
  revokeCookie,
  setCookieName,
  setCookieAuth,
  revokeCookieAuth,
  getCookie
} from "./config.js";

const login = () => {
  let email = $("#form_username").val();
  let password = $("#form_password").val();
  axios
    .post(
      auth.login,
      {
        email: email,
        password: password
      },
      config.unauthenticated
    )
    .then(function(response) {
      let token = response.data.success.token;
      let expires = response.data.success.expired_at;
      let role = response.data.success.role;
      console.log(token);
      setCookieAuth(token, expires);
      setCookieName("role", role, expires);
      if (role == "member") {
        redirectTo(route.member.index);
      } else if (role == "lecturer") {
        redirectTo(route.praetorian.index);
      } else if (role == "admin") {
        redirectTo(route.admin.index);
      }
    })
    .catch(function(error) {
      var err = document.getElementById("alert");
      err.style.display = "block";
      err.style.opacity = "100";
      $(".classForm").trigger("reset");
    });
};

const logout = () => {
  axios
    .get(auth.logout, config.auth)
    .then(function(response) {
      revokeCookieAuth();
      revokeCookie("role");
      redirectTo("index.html");
    })
    .catch(function(error) {
      console.log(error);
      revokeCookieAuth();
      revokeCookie("role");
      redirectTo("index.html");
    });
};

export { login, logout };
