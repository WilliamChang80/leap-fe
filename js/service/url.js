const host = "http://" + "localhost:8000";

//endpoint frontend
const route = {
  admin: {
    index: "pages/admin/index.html",
    "academic-calendar": "pages/admin/academic-calendar.html",
    "academic-calendar-list": "pages/admin/academic-calendar-list.html",
    "add-academic-calendar": "pages/admin/add-academic-calendar.html",
    "add-class": "pages/admin/add-class.html",
    "class-list": "pages/admin/class-list.html",
    "update-class": "pages/admin/update-class.html"
  },
  member: {
    index: "pages/member/member-page.html",
    "view-class": "pages/member/view-class.html",
    "view-material": "pages/member/view-material.html",
    "upload-project": "pages/member/upload-project.html",
    attendance: "pages/member/attendance.html",
    "my-account": "pages/member/my-account.html"
  },
  praetorian: {
    index: "pages/praetorian/index.html",
    "view-class": "pages/praetorian/class-list.html",
    attendance: "pages/praetorian/attendance.html",
  }
};

//ini endpoint
const auth = {
  login: host + "/api" + "/login",
  logout: host + "/api" + "/logout",
  details: host + "/api" + "/details",
  "change-email": host + "/api" + "/change-email",
  "change-password": host + "/api" + "/change-password"
};

const member = {
  details: host + "/api" + "/details",
  "previous-class": host + "/api" + "/class-schedule/previous-class",
  "next-class": host + "/api" + "/class-schedule/next-class",
  "all-class-material": host + "/api" + "/class-schedule/all-class-material",
  "get-user-assignment": host + "/api" + "/assignments/member/assignment",
  "submit-mid-project": host + "/api" + "/submit-assignment/mid",
  "submit-final-project": host + "/api" + "/submit-assignment/final",
  "user-classes-count": host + "/api" + "/class-schedule/class-count",
  "user-absences": host + "/api" + "/absences/",
  "user-absences-count": host + "/api" + "/absences/attendance-count"
};

const admin = {
  details: host + "/api" + "/details"
};

const lecturer = {
  details: host + "/api" + "/details",
  "class-list": host + "/api" + "/class-schedule/lecturer/next-class",
  "attendance": host + "/api" + "/absences/member",
  "check-attendance": host + "/api" + "/absences/check",
  "insert": host + "/api" + "/absences/insert"
};

export { auth, route, member, admin, lecturer, host };
