import {config} from '../service/config.js'
import {logout} from '../service/auth.js'
import {isLecturer} from '../service/middleware.js'
import { lecturer } from '../service/url.js'

const parseDateString = (s) => {
    var b = s.split(/\D/); 
    var d = new Date(b[0], --b[1], b[2]);
    const month = d.toLocaleString('default', { month: 'short' });
    return d.getDate() + " " + month + " " + d.getFullYear();
}

async function getLecturerData() {
	await axios.get(lecturer.details, 
		config.auth
	)
		.then((response) => {
        var lecturerName = response.data.success.name
        $('#nama-praetorian').html(lecturerName)
	}).catch(function (error) {
		console.log(error);
	});
}

const getLecturerClassList = () => {
	axios.get(lecturer["class-list"], 
		config.auth
	)
		.then((response) => {
            console.log(response.data.lecturer_classes[0].course_name)
        let course_name = response.data.lecturer_classes[0].course_name
        let b_time = response.data.lecturer_classes[0].next_classes[0].begin_time.split(":")
        let e_time = response.data.lecturer_classes[0].next_classes[0].end_time.split(":")
        let shift = b_time[0] + "." +b_time[1]+"-"+e_time[0] + "." +e_time[1]
        let meeting_name = response.data.lecturer_classes[0].next_classes[0].meeting_name
        let meeting_date = response.data.lecturer_classes[0].next_classes[0].meeting_date
        let d = parseDateString(meeting_date)
        let meeting_location = response.data.lecturer_classes[0].location
        $('#course-name').text(course_name)
        $('#meeting-date').text(d+", "+shift)
        $('#meeting-location').text(meeting_location)
        $('#meeting-name').text(meeting_name)
	}).catch(function (error) {
		console.log(error);
	});
}


$(document).ready(function() {
    if(!isLecturer()){
        alert("You Are Not Allowed to Access This Page")
        logout()
    } else {
        getLecturerData()
        getLecturerClassList()
        $('#btn-logout').click(function() {
            logout()
        });
    }
})

