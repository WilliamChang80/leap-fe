import { login, logout } from "./service/auth.js";
import { config, revokeCookieAuth } from "./service/config.js";

var flagEmail = 0;
var flagPassword = 0;

$("#form_username").click(function() {
  if (flagEmail == 0) {
    $("#form_username").before(
      '<div class="emailBaru"><label>Email</label></div>'
    );
    $("#form_username").css("opacity", "1");
    $(".emailBaru").css("color", "#4bc1ec");
    event.preventDefault();
    flagEmail = 1;
  }
});

$("#form_username").focus(function() {
  $(this).removeAttr("placeholder");
});

$("#form_password").click(function() {
  if (flagPassword == 0) {
    $("#form_password").before(
      '<div class="passwordBaru"><label>Password</label></div>'
    );
    $("#form_password").css("opacity", "1");
    $(".passwordBaru").css("color", "#4bc1ec");
    event.preventDefault();
    flagPassword = 1;
  }
});

$("#form_password").focus(function() {
  $(this).removeAttr("placeholder");
});

$(".classForm").on("submit", function() {
  login();
  return false;
});
