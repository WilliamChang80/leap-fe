$(document).ready(function() {
  var content = "";
  for (var i = 0; i < 10; i++) {
    var contentTemplate = ` <div class="col-10 col-md-6 col-lg-3">
                                                 <div id="ContentClass${i}" class="contentClass">
                                                        <div class="contentContainer">
                                                                <div class=""contentTittleContainer>
                                                                    <div class="contentTittle">
                                                                      <h2> Monday </h2>
                                                                      <div class="rightContentTittle">
                                                                      <h4> 07.20-09.00</h4>
                                                                      <i class="fas fa-angle-down fa-2x trigger not-press"></i>
                                                                      </div>
                                                                      </div>
                                                                      <h5> Kemanggisan </h5> 
                                                                    </div>
                                                               <div class="contents">
                                                                      <h5> Praetorian: Yosef Tanaka</h5>                                      
                                                               </div>
                                                               <div class="contents">
                                                                      <h5>Capacity : 8</h5>
                                                                      <h5>Registered Member : 0</h5>  
                                                               </div>
                                                               <div class="contentIcon">               
                                                                      <div class="contentIconBorder">
                                                                      <button type="button" class="btn icon" data-toggle="modal" data-target="#exampleModalCenter">
                                                                      <i class="fas fa-list "></i>
                                                                      </button>
                                                                      </div>
                                                    
                                                                      <div class="contentIconBorder">
                                                                      <a href="updateClass.html" class="icon">
                                                                      <i class="fas fa-edit"></i>
                                                                      </a>
                                                                      </div>

                                                                      <div class="contentIconBorder">
                                                                      <button type="button" class="btn icon" data-toggle="modal" data-target="#exampleModalCenter">
                                                                      <i class="fas fa-trash-alt"></i>
                                                                      </button>
                                                                       </div>
                                                               </div>
                                                        </div>
                                                 </div>
                                          </div> `;
    content += contentTemplate;
  }
  $("#containerRow").html(content);
  $(".listClass").click(function() {
    if ($(this).css("height") === "48px") {
      $(this).css({
        height: "240px",
        transition: "1s"
      });
      $(this)
        .find("i")
        .addClass("press");
      $(this)
        .find("i")
        .removeClass("not-press");
    } else {
      $(this).css({
        height: "48px",
        transition: "1s"
      });
      $(this)
        .find("i")
        .addClass("not-press");
      $(this)
        .find("i")
        .removeClass("press");
    }
  });
  var x = window.matchMedia("(min-width: 768px)");
  $(".contentClass").hover(
    function() {
      if (x.matches) {
        $(this).css({
          transform: "scale(1.1)",
          transition: "0.5s"
        });
        $(".contentClass")
          .not(this)
          .css({
            transform: "scale(0.9)",
            transition: "0.5s"
          });
        $(this)
          .find(".contentIcon")
          .css("display", "flex");

        $(this)
          .find(".icon")
          .hover(
            function() {
              $(this).css({
                backgroundColor: "white",
                cursor: "pointer"
              });
              $(this)
                .find("i")
                .css({
                  color: "#43C2FA"
                });
            },
            function() {
              $(this).css({
                backgroundColor: "#43C2FA",
                color: "white"
              });
              $(this)
                .find("i")
                .css({
                  color: "white"
                });
            }
          );

        if ($(window).width() < 768) {
          $(this)
            .find(".contentIcon")
            .css("display", "none");
        }
      }
    },
    function() {
      if (x.matches) {
        $(this).css({
          transform: "scale(1)",
          transition: "0.5s"
        });
        $(".contentClass")
          .not(this)
          .css({
            transform: "scale(1)",
            transition: "0.5s"
          });
        $(this)
          .find(".contentIcon")
          .css("display", "none");
      }
    }
  );

  $(".contentClass").click(function() {
    if ($(this).css("height") === "50px") {
      $(this).css({
        height: "200px",
        transition: "0.5s"
      });
      $(this)
        .find(".contentContainer")
        .css({
          height: "200px",
          display: "flex",
          justifyContent: "space-around"
        });
      $(this)
        .find(".contentTittle")
        .css({
          display: "flex",
          flexDirection: "row"
        });
      $(this)
        .find("h5")
        .css({
          display: "flex",
          flexDirection: "column",
          fontSize: "12px",
          marginBottom: "0"
        });
      $(this)
        .find(".contents")
        .css({
          display: "flex",
          flexDirection: "column"
        });

      $(this)
        .find(".contentIcon")
        .css({
          display: "flex",
          justifyContent: "space-around"
        });

      $(this)
        .find(".trigger")
        .addClass("press2");
      $(this)
        .find(".trigger")
        .removeClass("not-press");

      $(this)
        .find(".icon")
        .hover(
          function() {
            $(this).css({
              backgroundColor: "white",
              cursor: "pointer"
            });
            $(this)
              .find("i")
              .css({
                color: "#43C2FA"
              });
          },
          function() {
            $(this).css({
              backgroundColor: "#43C2FA",
              color: "white"
            });
            $(this)
              .find("i")
              .css({
                color: "white"
              });
          }
        );
    } else if ($(this).css("height") === "200px") {
      $(this).css({
        height: "50px",
        transition: "0.5s"
      });
      $(this)
        .find("h5")
        .css({
          display: "none"
        });
      $(this)
        .find(".contentContainer")
        .css({
          height: "50px",
          transition: "0.5s"
        });
      $(this)
        .find(".contents")
        .css({
          display: "none"
        });

      $(this)
        .find(".contentIcon")
        .css({
          display: "none"
        });
      $(this)
        .find(".trigger")
        .addClass("not-press");
      $(this)
        .find(".trigger")
        .removeClass("press2");
    }
  });

  $("#Cancel").click(function() {
    $("#exampleModalCenter").modal("hide");
  });
});
