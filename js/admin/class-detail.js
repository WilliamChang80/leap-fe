let responseHeader = {
    "status": 200,
    "message": "ok",
    "data":
        {
            "id": 11,
            "period": 2019,
            "term_id": 1,
            "course_id": 3,
            "lecturer_id": 5,
            "location_id": 1,
            "day": "Monday",
            "shift_id": 1,
            "capacity": 7,
            "term_name": "Odd Semester",
            "course_name": "Kamille Schuppe",
            "location_name": "Dianna Stokes",
            "lecturer_name": "Alexanne Bradtke",
            "lecturer_email": "jalyn52@example.net",
            "lecturer_phone_number": "000000000000",
            "class_begin_time": "07:20:00",
            "class_end_time": "09:00:00"
        }
    }
    
// funtion buat set data dari BE ke UI
function updateDataToUI(data) {
    let content = "";
    content = 
        `<div class="con_class_detail">
            <div class="d-flex">
                <h1 class="text_class_detail text-class-bold">${data.course_name}</h1>
            </div>
            <div class="text-splitter-number d-flex">
                <h3 class="text_class_detail text-class-semi-bold">${data.day}, ${data.class_begin_time} - ${data.class_end_time}</h3>
            </div>
            <div class="text-splitter-number d-flex">
                <h5 class="text_class_detail text-class-normal">Praetorian: ${data.lecturer_name}</h5>
            </div>
            <div class="text-splitter-number d-flex">
                <h5 class="text_class_detail text-class-normal">Capacity: ${data.capacity}</h5>
            </div>
            <div class="d-flex">
                <h5 class="text_class_detail text-class-normal">Registered Member: ${data.registered}</h5>
            </div>
        </div>`
    document.querySelector(".con_class_detail").innerHTML = content;
}

let responseCurriculum = {
    "status": 200,
    "message": "ok",
    "data":
        {
            "id": 11,
            "period": 2019,
            "term_id": 1,
            "course_id": 3,
            "lecturer_id": 5,
            "location_id": 1,
            "day": "Monday",
            "shift_id": 1,
            "capacity": 7,
            "term_name": "Odd Semester",
            "course_name": "Kamille Schuppe",
            "location_name": "Dianna Stokes",
            "lecturer_name": "Alexanne Bradtke",
            "lecturer_email": "jalyn52@example.net",
            "lecturer_phone_number": "000000000000",
            "class_begin_time": "07:20:00",
            "class_end_time": "09:00:00"
        }
    }

//pnaggil function
updateDataToUI(responseHeader.data);