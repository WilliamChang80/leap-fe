import { config } from "../service/config.js";
import { logout } from "../service/auth.js";
import { isAdmin } from "../service/middleware.js";
import { lecturer } from "../service/url.js";

async function getLecturerData() {
  await axios
    .get(lecturer.details, config.auth)
    .then(response => {
      var lecturerName = response.data.success.name;
      $("#nama-praetorian").html(lecturerName);
    })
    .catch(function(error) {
      console.log(error);
    });
}

$(document).ready(function() {
  if (!isAdmin()) {
    alert("You Are Not Allowed to Access This Page");
    logout();
  } else {
    getLecturerData();

    $("#btn-logout").click(function() {
      logout();
    });
  }
  $("#bar").click(function() {
    $(".navItemContainer").toggleClass("activeBar");
  });
});
