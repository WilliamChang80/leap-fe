const prevArrorw = $(".carousel-control-prev");
const nextArrow = $(".carousel-control-next");
const carouselContainer = document.querySelectorAll(
  "#carouselId>.carousel-inner>.carousel-item"
);
let activeIndex;
carouselContainer.forEach((elem, index) => {
  if (elem.classList.length === 2) {
    activeIndex = index + 1;
  }
});

const arrayOfCarouselItem = [
  carouselContainer[3],
  ...carouselContainer,
  carouselContainer[0]
];

nextArrow.removeAttr("data-slide href");
prevArrorw.removeAttr("data-slide href");

nextArrow.click(() => {
  arrayOfCarouselItem[activeIndex + 1].children[0].click();
});

prevArrorw.click(() => {
  arrayOfCarouselItem[activeIndex - 1].children[0].click();
});
