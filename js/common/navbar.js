import { logout } from "../service/auth.js";
import { config, redirectTo, urlTo } from "../service/config.js";
import { member, route } from "../service/url.js";
import { isMember, isAdmin, isLecturer } from "../service/middleware.js";

$(document).ready(function() {
  $("#bar").click(function() {
    $(".navItemContainer").toggleClass("activeBar");
  });

  if (isMember()) {
    $("#logo").attr("href", urlTo(route.member.index));

    $("#btn-home").attr("href", urlTo(route.member.index));

    $("#btn-my-account").attr("href", urlTo(route.member["my-account"]));

    $("#btn-view-class").attr("href", urlTo(route.member["view-class"]));
  } else if (isLecturer()) {
    $("#logo").attr("href", urlTo(route.praetorian.index));

    $("#btn-home").attr("href", urlTo(route.praetorian.index));

    $("#btn-view-class").attr("href", urlTo(route.praetorian["view-class"]));
  } else if (isAdmin()) {
    $("#logo").attr("href", urlTo(route.admin.index));

    $("#btn-home").attr("href", urlTo(route.admin.index));

    // $("#btn-view-course").attr("href", urlTo(route.admin.index))

    $("#btn-view-class").attr("href", urlTo(route.admin["class-list"]));

    // $("#btn-view-people").attr("href", urlTo(route.admin.index))

    // $("#btn-view-backend").attr("href", urlTo(route.admin.index))
  }
  $("#btn-logout").click(function() {
    logout();
  });
});
